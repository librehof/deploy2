#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib

DISTRO="ubuntu22"
ARCH="amd64"
RELEASE="jammy"
SOURCE="http://us.archive.ubuntu.com/ubuntu"

ROOTDIR="/mnt/rootfs"
TMPDIR="${TMPPATH}/${DISTRO}${ARCH}cache"
TMPDISK="${TMPPATH}/${DISTRO}${ARCH}strap.disk"

#=================================================

# ubuntu22amd64strap/make-rootfs * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Build artifacts/${DISTRO}${ARCH}strap.rootfs.sqfs"
  info ""
  info "Usage:"
  info " ${SCRIPT} [cache] [--no-check-gpg]"
  info " ${SCRIPT} cleanup"
  info ""
  info "Will use debootstrap (downloading from ${SOURCE})"
  info ""
  info "MOUNT: ${ROOTDIR}"
  info "TMP:   ${TMPDIR}"
  info "IMAGE: ${TMPDISK}"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

CACHE="false"
if [ "${1}" = "cache" ]; then
  CACHE="true"
  FLAGS="${2}"
else
  FLAGS="${1}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}
checkdir "checks"
exitonerror ${?}

checksuperuser
exitonerror ${?}

if [ "${1}" = "cleanup" ]; then
  action "Cleanup"
  rootfs-unmount "${ROOTDIR}"
  if [ -e "${TMPDISK}" ]; then
    image-detach "${TMPDISK}"
    ensurenofile "${TMPDISK}" note
  fi
  exit
fi

#-------------------------------------------------

DEBCACHETGZ="artifacts/${DISTRO}${ARCH}strap.cache.tgz"
DEBCACHELOG="checks/${DISTRO}${ARCH}strap.cache.log"

ROOTSQFS="artifacts/${DISTRO}${ARCH}strap.rootfs.sqfs"
ROOTSHA="checks/${DISTRO}${ARCH}strap.rootfs.sqfs.sha1"
ROOTFSLOG="checks/${DISTRO}${ARCH}strap.rootfs.log"

INSTALL="linux-base,locales,tzdata,binutils,apt-utils,zlib1g,gzip,zip,unzip,xz-utils,lsof,nano,btrfs-progs"

#-------------------------------------------------

if [ -e "${ROOTDIR}" ]; then
  errorexit "Already exists: ${ROOTDIR} (rootfs-unmount ${ROOTDIR})"
fi

ensurenodir "${TMPDIR}" note
exitonerror ${?}

if [ -e "${TMPDISK}" ]; then
  image-detach "${TMPDISK}"
  exitonerror ${?}
  ensurenofile "${TMPDISK}" note
  exitonerror ${?}
fi

if [ -e "${ROOTSQFS}" ]; then
  errorexit "Already exists: ${ROOTSQFS}"
fi

ensurenofile "${ROOTSHA}"
exitonerror ${?}

ensurenofile "${ROOTFSLOG}"
exitonerror ${?}

#-------------------------------------------------

# use debootstrap to setup minimal distrofs
ensurecmd debootstrap @ debootstrap
exitonerror ${?}

#-------------------------------------------------

div

note "DISTRO:  ${DISTRO} (${RELEASE})"
note "ARCH:    ${ARCH}"
note "SOURCE:  ${SOURCE}"
note "INSTALL: ${INSTALL}"
note "FLAGS:   ${FLAGS}"

div

info "Press ENTER or wait for 10 seconds"
keywait 10

#-------------------------------------------------

# cache

div

if [ "${CACHE}" = "false" ]; then
  note "Clearing cache"
  ensurenofile "${DEBCACHELOG}"
  exitonerror ${?}
  ensurenofile "${DEBCACHETGZ}"
  exitonerror ${?}
fi

if [ -f "${DEBCACHETGZ}" ]; then

  TIMESTAMP="$(date "+%Y-%m-%d" -r "${DEBCACHETGZ}")"
  note "Reusing ${DEBCACHETGZ} (${TIMESTAMP})"
  keywait 3

else

  action "Pre-strapping: ${DEBCACHETGZ}"

  DEBCACHETGZ="$(abspath "${DEBCACHETGZ}")"
  exitonerror ${?}

  ensuredir "${TMPDIR}"
  exitonerror ${?}

  ensurenofile "${DEBCACHELOG}"
  exitonerror ${?}

  ensurenofile "${DEBCACHETGZ}"
  exitonerror ${?}

  debootstrap --arch "${ARCH}" --include "${INSTALL}" ${FLAGS} "--make-tarball=${DEBCACHETGZ}" "${RELEASE}" "${TMPDIR}" "${SOURCE}" > "${DEBCACHELOG}" 2> "${DEBCACHELOG}"
  STATUS=${?}

  inheritowner "${DEBCACHETGZ}"
  inheritowner "${DEBCACHELOG}"
  ensurenodir "${TMPDIR}"
  exitonerror ${?}

  if [ "${STATUS}" -gt 1 ]; then
    notetail "${DEBCACHELOG}"
    errorexit "Failed to make debootstrap tarball (${STATUS})"
  fi

  if [ "${STATUS}" = 1 ]; then
    echo "------- status -------"
    grep "W: " "${DEBCACHELOG}"
    grep "E: " "${DEBCACHELOG}"
    echo "-------- end ---------"
    keywait 3
    FOUND="$(grep "Deleting target directory" "${DEBCACHELOG}")"
    if [ "${FOUND}" = "" ]; then
      notetail "${DEBCACHELOG}"
      errorexit "Failed to make debootstrap tarball (${STATUS})"
    fi
  fi

fi

#-------------------------------------------------

div

action "Mounting:"

image-create "${TMPDISK}" 2000M
exitonerror ${?}

LOOPDEV="$(image-attach "${TMPDISK}")"
exitonerror ${?}

format-ext4 "${LOOPDEV}" "${DISTRO}"
exitonerror ${?}

dirmount-ext "${ROOTDIR}" "${LOOPDEV}"
exitonerror ${?}

#-------------------------------------------------

div

action "Strapping: ${DISTRO}${ARCH}"

if [ -f "${DEBCACHELOG}" ]; then
  cp "${DEBCACHELOG}" "${ROOTFSLOG}"
  exitonerror ${?} "Failed to copy ${DEBCACHELOG}"
  appendline "" "${ROOTFSLOG}"
  exitonerror ${?}
else
  clearfile "${ROOTFSLOG}"
  exitonerror ${?}
fi

DEBCACHETGZ="$(abspath "${DEBCACHETGZ}")"
exitonerror ${?}

debootstrap --arch "${ARCH}" --include "${INSTALL}" ${OPTION} "--unpack-tarball=${DEBCACHETGZ}" "${RELEASE}" "${ROOTDIR}" "${SOURCE}" >> "${ROOTFSLOG}" 2>> "${ROOTFSLOG}"
STATUS=${?}

inheritowner "${ROOTFSLOG}"
exitonerror ${?}

if [ ${STATUS} != 0 ]; then
  notetail "${ROOTFSLOG}"
  errorexit "Failed to strap ${DISTRO}${ARCH} at ${ROOTDIR} (exit ${STATUS})"
fi

#-------------------------------------------------

div

action "Post-strapping:"

clearfile "${ROOTDIR}/etc/fstab"
exitonerror ${?}

clearfile "${ROOTDIR}/etc/crypttab"
exitonerror ${?}

# remove resolv.conf if not a link
if [ -e "${ROOTDIR}/etc/resolv.conf" ] && [ ! -L "${ROOTDIR}/etc/resolv.conf" ]; then
  ensurenofile "${ROOTDIR}/etc/resolv.conf" note
  exitonerror ${?}
fi

#-------------------------------------------------

div

action "Squashing:"

dir-squash "${ROOTDIR}" "${ROOTSQFS}"
exitonerror ${?}

HASH="$(sha1hash "${ROOTSQFS}")"
exitonerror ${?}

saveline "${HASH}" "${ROOTSHA}"
exitonerror ${?}

#-------------------------------------------------

div

action "Cleanup:"

dirmount-remove "${ROOTDIR}"
image-detach "${TMPDISK}"
ensurenofile "${TMPDISK}" note

#-------------------------------------------------
