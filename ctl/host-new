#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib
. "${SCRIPTPATH}/lib"

#=================================================

# deploy2/host-new * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Create host from generic"
  info ""
  info "Usage:"
  info " ${SCRIPT} <genericdir> <hostdir> [noroot]"
  info ""
  info "Example:"
  info " ctl/${SCRIPT} deploy2/generic/debian11host hosts/myhost"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}

GENDIR="${1}"
HOSTDIR="${2}"
NOROOT="${3}"

if [ "${GENDIR}" = "" ] || [ "${HOSTDIR}" = "" ] || [ "${4}" != "" ]; then
  inputexit
fi

GENDIR="$(abspath "${GENDIR}")"
exitonerror ${?}

checkdir "${GENDIR}"
exitonerror ${?}

HOSTDIR="$(abspath "${HOSTDIR}")"
exitonerror ${?}

if [ -e "${HOSTDIR}" ]; then
  errorexit "Already present: ${HOSTDIR}"
fi

#-------------------------------------------------

# copy

dir-copy "${GENDIR}" "${HOSTDIR}"
exitonerror ${?}

#-------------------------------------------------

# doc/<host>.md

if [ ! -e "${HOSTDIR}/doc" ]; then
  HOST="$(basename "${HOSTDIR}")"
  action "Creating: doc/@${HOST}.md"
  sleep 1
  ensuredir "${HOSTDIR}/doc"
  exitonerror ${?}
  saveline "# ${HOST}" "${HOSTDIR}/doc/@${HOST}.md"
  exitonerror ${?}
fi

#-------------------------------------------------

# inherit

if [ -f "${HOSTDIR}/inherit" ]; then
  div
  note "inherit"
  chmod +x "${HOSTDIR}/inherit"
  "${HOSTDIR}/inherit"
fi

#-------------------------------------------------

# newid

if [ -e "${HOSTDIR}/host.conf" ]; then
  div
  ensuredir "${HOSTDIR}.secret"
  exitonerror ${?}
  "${SCRIPTPATH}/host-newid" "${HOSTDIR}" "${NOROOT}"
fi

#-------------------------------------------------
