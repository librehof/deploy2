## Deploy 2 | Change Log

GPLv3 (C) 2023 [librehof.org](http://librehof.org) [(top)](/README.md)

2024-04-10 (v2.52)
- added generic/win11diskdialog
- multi-init "old" option (to create inactive disk)

2023-11-02 (v2.51)
- support for home directory sync

2023-09-22 (v2.50)
- requires supersys v1.60+
- new: ctl/multi-switchid
- mirrorid fix

2023-09-10 (v2.43)
- multi-init and multi-mirror with default sector size

2023-09-09 (v2.42)
- default LOGMIBS=500
- ctl/vdisk-breakout: directory to breakout given on command line
- ctl/multi-mount minor fix
- ctl/rootfs-strap minor fix

2023-08-24 (v2.41)
- added LINUXPARAMS
- generic hosts with: `LINUXPARAMS="mitigations=off"`
- journald.conf handling at the end of strap

2023-08-24 (v2.40)
- SCHEMA instead of SINGLE and MULTI (!)
- New schema: lukszfs@efisd
- New ENDMIBS auto-parameter (free space left at disk end)
- New INSTANCES parameter (no longer part of machine name)
- Support for sector=n (multi-init/mirror)
- Default host doc at `doc/@<host>.md`

2023-06-20 (v2.31)
- /inititate renamed to /prepare
- ensure /etc/machine-id (with /var/lib/dbus/machine-id link)

2023-06-18 (v2.30)
- added debian 12 (now deployer default)
- writing TIMEZONE into /etc/timezone
- multi-mirror secondary volid fix
- dir-round wininst fat32
- swap "nofail"

2023-06-09 (v2.22)
- swap "nofail" only if mirror
- removed "noexec" from RAM /tmp

2023-06-08 (v2.21)
- zfs mirror support
- hostx (containing experimental hosts)

2023-06-01 (v2.20)
- new host groups: 
  - host\[s|v|x|c\] 
  - s=server | v=virtual | c=client
  - vhosts is deprecated (now hostv)
- generic ubuntu: added netplan yaml (will activate NetworkManager)

2023-05-28 (v2.10)
- rootfs-efisdboot (requiring supersys 1.37)
- default deployer/stage includes artifacts/firmware directory

2023-05-25 (v2.9)
- generic/linuxhost

2023-05-23 (v2.8)
- ctl/host => ctl/interact
- inherit fix
- backup secret check
- default menutime=3 (multifs)

2023-05-11 (v2.7)
- inherit will clear all variables before generating host.conf
- generic hosts includes zz-sdboot initramfs/kernel hook scripts 

2023-05-09 (v2.6)
- some adjustments like kernel log-level (sysctl.conf)

2023-05-06 (v2.5)
- split /activate into (multi): 
  - /initiate (rootfs-efisd) 
  - /activate (rootfs-efisdupdate)
- xdisk-create multi-side machine disks: 
  - \<machine\>-AB: \<machine\>-A.disk and \<machine\>-B.disk  

2023-05-05 (v2.4)
- luks discard
- machine-new
- sources.list includes security updates

2023-04-27 (v2.3)
- fixed winhost bug (blank WINKEY)
- make-wininst help text
- \[x\]host push, calls inherit first

2023-04-20 (v2.2)
- encryption brute-force message
- changed default swap size

2023-04-16 (v2.1)
- working systemd-boot
- working debian with zfs

2023-02-23 (v2.0)
- remake of deploy 1
