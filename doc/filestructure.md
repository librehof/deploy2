# Deploy 2 | File Structure

GPLv3 (C) 2023 [librehof.org](http://librehof.org) [(top)](/README.md)
<br>

## Deployment base directory (your git repo)

[*] = mandatory

```
ctl/            => deploy<N>/ctl/
  
setup           # setup [latest] - run after cloning repo, or to update
inherit         # build common files and call all <host>/inherit
backup          # backup => archive/<date>-<base>.7z
  
.gitignore      # filter sub-git repos, artifacts etc.  
```
```
archive/        # local backups
```
``` 
artifacts/      # [*] backup to a mass-storage area, not in git!

  <installer-ver-arch>.iso
  <winverarchtype>.wininst.iso
  
  <distroverarchtype>.rootfs.sqfs (squashfs)
  
  <custom files>
```
```
checks/         # [*] artifact checksums
```
```
config/         # [*] common config (part of your git repo)
  
  global.conf         # [X]VARIANT
  local.conf          # global.conf override (not in git)
  global.<variant>    # variant specific - must not set [X]VARIANT
  local.<variant>     # global.<variant> override (not in git)
```
```
hardware/       # inventory (part of your git repo)
  <model>/         
     manual.pdf       # hardware manual
```
```
machines/       # inventory (part of your git repo)
  <machine>/
    machine.conf      # [*]
    machine.<variant>
    machine.md
```
```
generic/        # your generic hosts (part of your git repo)        
  <host>/         # linux host
  <winhost>/      # windows host
``` 
```
host[s|v|c|x]/    # your hosts (part of your git repo)

  [s=server|v=virtual|c=client|x=experimental]
  
  <host>/         # linux host <=> /deploy
    doc/@<host>.md    # host documenation (top-doc or all-in-one)
    
    rootfs/           # common on-top-of artifact => /deploy/rootfs 
    rootfs.7z         # common secrets (AES-encrypted) => /deploy.secret/rootfs
    rootfs.<variant>/ # variant on-top-of common
    rootfs.<variant>.7z # variant secrets

    home/             # common home => /deploy/home
    home.<variant>/   # variant home => /deploy/home

    host.conf         # [*]
    host.<variant>    # variant override
    
    stage             # custom base-to-rootfs copy before strap
    stage<N>          # additonal stage, called inline at end of stage
    
    strap             # [*] "chroot" setup (admin, ssh, ...)
    strap.list        # packages installed by strap (using sw-include)
    strap<N>          # additonal strap, called inline at end of strap
    strap<N>.list     # installed before strap<N> by strap
    
    nocow.list        # list with "nocow" vm and db directories (btrfs specific)    
    
    setup             # setup [initial] - on-the-host setup (services, ...)
    setup.list        # packages installed by setup (using sw-include)
    setup<N>          # additonal setup, called inline at end of setup
    setup<N>.list     # installed before setup<N> by setup
    
    delta             # delta [pushsame|pullsame] - diff /deploy with rootfs
    sync              # sync [push|missing|pull] - sync /deploy <-> rootfs
    sync<N>           # called inline by sync at the end (if not pull)
    
  <winhost>/      # windows host => installer usb-image
    doc/@<host>.md    # host documenation (top-doc or all-in-one)
  
    winhost.conf      # [*]
    winhost.<variant> # variant override    
    autounattend.xml  # autounattend template (pick from generic)
        
  deployer/       # special usb-boot host, for deploying linux hosts on live machines 
    stage             # [*] will stage /deployer dir (filtered copy of base)
```
``` 
deploy<N>/      # sub-git-repo - deploy control (librehof.org)

  init            # initiate base above (first-time setup of your git repo) 
  ctl/            # scripts launched (-h for help) from base via ctl link 
    <commands>
  generic/        # generic hosts
    <host>/         # linux host
    <winhost>/      # windows host  
  build/ 
    <distroverarchtype>/
      build-rootfs
    <winverarchtype>/
      build-wininst
```
```
supersys<N>/    # sub-git-repo - used by deploy scripts (librehof.org)
  install         # install to /bin
```
```
xdev/           # dev images to launch with qemu/kvm (not in git)
  <machine>.disk
  <host>.disk
  <WINHOST>.usb
```
