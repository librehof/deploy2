#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib
. "${SCRIPTPATH}/lib"

TMPHOST="${TMPPATH}/winhost"

#=================================================

# deploy2/winhost-to-isoinst * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Make winhost DVD-image installer (for legacy BIOS)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <hostdir> <isofile>"
  info " ${SCRIPT} cleanup"
  info ""
  info " <hostdir>: host[s|v|x|c]/<winhost>"
  info ""
  info "Temporary mounts:"
  info " /mnt/wininst"
  info " ${TMPHOST}"
  info ""
  info "Examples:"
  info " ${SCRIPT} hosts/somehost /var/lib/libvirt/images/WINHOST.iso"
  info ""
}

# TODO: xdisk version

#=================================================

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}

checksuperuser
exitonerror ${?}

if [ "${1}" = "cleanup" ] && [ "${2}" = "" ]; then
  dirmount-remove "/mnt/wininst"
  ensurenodir "${TMPHOST}"
  "${SCRIPTPATH}/xdisk-detach" "all"
  exit 0
fi

#-------------------------------------------------

# input

HOSTDIR="${1}"
TARGET="${2}"

if [ "${HOSTDIR}" = "" ] || [ "${TARGET}" = "" ]; then
  inputexit
fi

if [ -e "/mnt/wininst" ]; then
  errorexit "Already mounted: /mnt/wininst (cleanup)"
fi

if [ -e "${TMPHOST}" ]; then
  warning "Already exists: ${TMPHOST}"
  keywait 3
fi

HOSTDIR="$(abspath "${HOSTDIR}")"
exitonerror ${?}

checkdir "${HOSTDIR}"
exitonerror ${?}

checkfile "${HOSTDIR}/autounattend.xml"
exitonerror ${?}

#-------------------------------------------------

STORAGE="vdisk"

# pre-load config (without wininst.conf)

loadcommon "${STORAGE}"
exitonerror ${?}

loadentity "winhost" "${HOSTDIR}"
exitonerror ${?}

LABEL="$(upperstring "${WINHOST}")"

if [ "${LABEL}" = "" ]; then
  errorexit "Blank LABEL"
fi

ISOFILE="artifacts/${WININST}.wininst.iso"

ISOFILE="$(abspath "${ISOFILE}")"
exitonerror ${?}

checkfile "${ISOFILE}"
exitonerror ${?}

#-------------------------------------------------

div

note "HOSTDIR:  ${HOSTDIR} (${WININST})"
note "TARGET:   ${TARGET} (${VARIANT})"
note "ISOFILE:  ${ISOFILE} (source)"
note "LABEL:    ${LABEL}"

div

info "Press ENTER or wait for 25 seconds"
keywait 25

#-------------------------------------------------
separator "Open"

checkfile "${ISOFILE}"
exitonerror ${?}

ISODEV="$(image-attach "${ISOFILE}")"
exitonerror ${?}

note "ISO loop device: ${ISODEV}"
sleep 1

dirmount-iso "/mnt/wininst" "${ISODEV}" ro
exitonerror ${?}

if [ ! -f "/mnt/wininst/sources/install.esd" ]; then
  errorexit "Missing /mnt/wininst/sources/install.esd"
fi

note "Found sources/install.esd"

#-------------------------------------------------
separator "Copy"

ensuredir "${TMPHOST}"
exitonerror ${?}

dir-replicate "/mnt/wininst" "${TMPHOST}"
exitonerror ${?}

div

checkfile "${TMPHOST}/wininst.conf"
exitonerror ${?}

# load config again (backed by wininst.conf)

loadcommon "${STORAGE}" "/mnt/fat32inst/wininst.conf"
exitonerror ${?}

loadentity "winhost" "${HOSTDIR}"
exitonerror ${?}

WINHOST="${HOST}"

note "WINHOST:   ${WINHOST}"
note "WINMIBS:   ${WINMIBS}"
note "WINUSER:   ${WINUSER}"
note "WINPASS:   ${WINPASS}"
note "WINLANG:   ${WINLANG}"
note "WINFORMAT: ${WINLANG}"
note "WININPUT:  ${WININPUT}"
note "WINKEY:    ${WINKEY}"

div

if [ "${WINKEY}" = "" ]; then
  warning "Blank WINKEY"
fi

info "Press ENTER or wait for 25 seconds"
keywait 25

#-------------------------------------------------

# generate autounattend.xml

UNATTENDED="${TMPHOST}/autounattend.xml"
TEMPLATE="${HOSTDIR}/autounattend.xml"

action "Compiling autounattend.xml"
sleep 1

cp "${TEMPLATE}" "${UNATTENDED}"
exitonerror ${?}

replaceinfile '${WINHOST}' "${WINHOST}" "${UNATTENDED}"
replaceinfile '${WINUSER}' "${WINUSER}" "${UNATTENDED}"
replaceinfile '${WINPASS}' "${WINPASS}" "${UNATTENDED}"
replaceinfile '${WINLANG}' "${WINLANG}" "${UNATTENDED}"
replaceinfile '${WINFORMAT}' "${WINFORMAT}" "${UNATTENDED}"
replaceinfile '${WININPUT}' "${WININPUT}" "${UNATTENDED}"
replaceinfile '${WINMIBS}' "${WINMIBS}" "${UNATTENDED}"
replaceinfile '${WINKEY}' "${WINKEY}" "${UNATTENDED}"

# cleanup

ensurenofile "${TMPHOST}/wininst.conf"
exitonerror ${?}

#-------------------------------------------------
separator "Make"

BASEPATH="$(pwd)"

ensurecmd mkisofs @ genisoimage
exitonerror ${?}

clearfile "${STDFILE}"
exitonerror ${?}

OUTFILE="${TARGET}"
ensurenofile "${OUTFILE}"
exitonerror ${?}

cd "${TMPHOST}"
exitonerror ${?}

mkisofs \
  -iso-level 2 \
  -V "${HOST}" \
  -J -l -D -N -joliet-long -udf \
  -relaxed-filenames -allow-limited-size \
  -b "boot/etfsboot.com" \
  -no-emul-boot -boot-load-size 8 -boot-load-seg 0x07C0 \
  -o "${OUTFILE}" \
  . 1> "${STDFILE}" 2> "${STDFILE}"

STATUS="${?}"

cd "${BASEPATH}"
exitonerror ${?}

inheritowner "${OUTFILE}"
exitonerror ${?}

notetail "${STDFILE}" 6
exitonerror ${STATUS} "Failed to make new ISO file"

SIZE="$(filesize "${OUTFILE}")"
SIZEINFO="$(sizeinfo "${SIZE}")"
exitonerror ${?}

div

note "Output: ${OUTFILE}"
note "Size: ${SIZEINFO}"
sleep 1

#-------------------------------------------------
separator "Unmount"

ensurenodir "${TMPHOST}"
dirmount-remove "/mnt/wininst"
image-detach "${ISODEV}"

#-------------------------------------------------
