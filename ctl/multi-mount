#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib
. supersys
. "${SCRIPTPATH}/lib"

#=================================================

# deploy2/multi-mount * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Mount multifs"
  info ""
  info "WARNING: Disk must NOT be in use (!)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <diskref> [<hostname>]"
  info ""
  info " <diskref>:"
  info " - disk device (/dev)"
  info " - raw image file (.raw .disk)"
  info " - virtual image file (.qcow2 .vdi .vmdk)"
  info ""
  info "Mounts:"
  info " /mnt/multifs"
  info " /mnt/rootfs # if <hostname>"
  info ""
}

#=================================================

# input

ROOTDIR="/mnt/rootfs"
MULTIDIR="/mnt/multifs"

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}

TARGET="${1}"
HOST="${2}"

if [ "${TARGET}" = "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

if [ -e "${ROOTDIR}" ]; then
  errorexit "Already mounted: ${ROOTDIR}"
fi

if [ -e "${MULTIDIR}" ]; then
  errorexit "Already mounted: ${MULTIDIR}"
fi

TARGET="$(abspath "${TARGET}")"
exitonerror ${?}

if [ ! -e "${TARGET}" ]; then
  errorexit "Missing ${TARGET}"
fi

#-------------------------------------------------

len="$(stringlen "${TARGET}")"
devdir="$(startstring "${TARGET}" 5)"
xdir="$(dirname "${TARGET}")"
basedir="$(dirname "${xdir}")"

if [ "${devdir}" = "/dev/" ] || [ -b "${TARGET}" ]; then
  STORAGE="disk"
else
  if [ "${basedir}" = "$(pwd)" ]; then
    STORAGE="xdisk"
  else
    STORAGE="vdisk"
  fi
fi

loadcommon "${STORAGE}"
exitonerror ${?}

if [ "${STORAGE}" = "xdisk" ]; then
  if [ "$(abspath "${XDISKDIR}")" != "${xdir}" ]; then
    errorexit "Expected xdisk path: ${XDISKDIR} (${xdir})"
  fi
fi

#-------------------------------------------------

DISKDEV=""
DISKDEV2=""

if [ "${STORAGE}" = "disk" ]; then
  DISKDEV="${TARGET}"
  ATTACH=""
else
  len="$(stringlen "${TARGET}")"
  disk="$(endstring "${TARGET}" $((len-4)))"
  raw="$(endstring "${TARGET}" $((len-3)))"
  if [ "${disk}" = ".disk" ] || [ "${raw}" = ".raw" ]; then
    note "Raw image"
    ATTACH="image"
    DISKDEV="$(image-attach "${TARGET}" reuse)"
    exitonerror ${?}
    if [ -f "${TARGET}2" ]; then
      # mirror
      DISKDEV2="$(image-attach "${TARGET}2" reuse)"
      exitonerror ${?}
    fi
  else
    note "Virtual image"
    ATTACH="vimage"
    DISKDEV="$(vimage-attach "${TARGET}" unsafe)"
    exitonerror ${?}
  fi
fi

note "Disk: ${DISKDEV}"

checkdevice "${DISKDEV}"
exitonerror ${?}

#-------------------------------------------------

scanmultiefi "${DISKDEV}"

if [ "${EFIDEV}" = "" ]; then
  errorexit "Could not find EFI/ESP partition (fat32)"
fi

if [ "${MULTIDEV}" = "" ]; then
  errorexit "Could not find MULTIFS partition"
fi

div

#-------------------------------------------------

# summary

note "TARGET:   ${TARGET} (${STORAGE})"
note "EFIDEV:   ${EFIINFO}"
note "MULTIDEV: ${MULTIINFO}"
if [ "${SWAPDEV}" != "" ]; then
note "SWAPDEV:  ${SWAPINFO}"
fi

div

sleep 1

#-------------------------------------------------

# mount multifs

if [ "${MULTICRYPT}" = "luks" ]; then
  OPENDEV="$(device-opencrypt "${MULTIDEV}")"
  exitonerror ${?}
else
  OPENDEV="${MULTIDEV}"
fi

FSTYPE="$(vol-type "${OPENDEV}")"
if [ "${FSTYPE}" = "zfs" ]; then
  # zfs
  dirmount-zfs "${MULTIDIR}" "${OPENDEV}"
  exitonerror ${?}
  SUBVOLS="$(subvol-list "${OPENDEV}" nosnapshots)"
  exitonerror ${?}
  if [ "${SUBVOLS}" != "" ]; then
    note "Mounting subvols inside multifs (zfs) ..."
    sleep 1
  fi
  for SUBVOL in ${SUBVOLS}; do
    dirmount-zfs "${MULTIDIR}/${SUBVOL}" "${OPENDEV}:${SUBVOL}"
    exitonerror ${?}
  done
else
  # btrfs
  dirmount-btrfs "${MULTIDIR}" "${OPENDEV}"
  exitonerror ${?}
fi

#-------------------------------------------------

# mount rootfs

if [ "${HOST}" != "" ]; then
  div
  rootfs-mount "${ROOTDIR}" "${MULTIDEV}" "subvol=${HOST}" "efi=${EFIDEV}" bind resolv
fi

#-------------------------------------------------
