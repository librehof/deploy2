#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib
. "${SCRIPTPATH}/lib"

#=================================================

# deploy2/winhost-to-fat32inst * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Make winhost USB installer (UEFI fat32 disk or image)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <hostdir> <diskdev> [<mibs>] [noformat]"
  info " ${SCRIPT} <hostdir> <diskimage> [<mibs>] [noformat]"
  info " ${SCRIPT} <hostdir> xdisk [<mibs>] [noformat]"
  info " ${SCRIPT} cleanup"
  info ""
  info " <hostdir>: host[s|v|x|c]/<winhost>"
  info " xdisk => xdev/<WINHOST>.usb (diskimage)"
  info ""
  info "Will generate autounattend.xml based on config"
  info ""
  info "Default FAT32 partition is 7700 MiB (mibs)"
  info ""
  info "When using windows usb installer with QEMU/KVM:"
  info "- add storage: SATA or USB (not VirtIO)"
  info "- boot, install, remove installer storage"
  info ""
  info "Temporary mounts:"
  info " /mnt/wininst"
  info " /mnt/fat32inst"
  info ""
  info "Examples:"
  info " ${SCRIPT} hosts/somehost /dev/sdx"
  info " ${SCRIPT} hosts/somehost xdisk"
  info ""
}

#=================================================

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}

checksuperuser
exitonerror ${?}

if [ "${1}" = "cleanup" ] && [ "${2}" = "" ]; then
  dirmount-remove "/mnt/wininst"
  dirmount-remove "/mnt/fat32inst"
  "${SCRIPTPATH}/xdisk-detach" "all"
  exit 0
fi

#-------------------------------------------------

# input

HOSTDIR="${1}"
TARGET="${2}"
NOFORMAT=""
if [ "${3}" != "noformat" ]; then
  MIBS="${3}"
  NOFORMAT="${4}"
else
  NOFORMAT="${3}"
  MIBS="${4}"
fi

if [ "${HOSTDIR}" = "" ] || [ "${TARGET}" = "" ]; then
  inputexit
fi

if [ "${NOFORMAT}" != "" ] && [ "${NOFORMAT}" != "noformat" ]; then
  inputexit
fi

if [ -e "/mnt/wininst" ]; then
  errorexit "Already mounted: /mnt/wininst (cleanup)"
fi

if [ -e "/mnt/fat32inst" ]; then
  errorexit "Already mounted: /mnt/fat32inst (cleanup)"
fi

HOSTDIR="$(abspath "${HOSTDIR}")"
exitonerror ${?}

checkdir "${HOSTDIR}"
exitonerror ${?}

checkfile "${HOSTDIR}/autounattend.xml"
exitonerror ${?}

if [ "${MIBS}" = "" ]; then
  MIBS=7700
fi

checkint "${MIBS}" space
exitonerror ${?}

#-------------------------------------------------

devdir="$(startstring "${TARGET}" 5)"
xdir="$(abspath "${TARGET}")"
xdir="$(dirname "${xdir}")"
basedir="$(dirname "${xdir}")"

if [ "${devdir}" = "/dev/" ] || [ -b "${TARGET}" ]; then
  STORAGE="disk"
elif [ "${basedir}" = "$(pwd)" ]; then
  errorexit "Use 'xdisk' not ${TARGET}"
elif [ "${TARGET}" = "xdisk" ]; then
  STORAGE="xdisk"
else
  STORAGE="vdisk"
fi

# pre-load config (without wininst.conf)

loadcommon "${STORAGE}"
exitonerror ${?}

loadentity "winhost" "${HOSTDIR}"
exitonerror ${?}

LABEL="$(upperstring "${WINHOST}")"

if [ "${LABEL}" = "" ]; then
  errorexit "Blank LABEL"
fi

ISOFILE="artifacts/${WININST}.wininst.iso"
ISOFILE="$(abspath "${ISOFILE}")"
exitonerror ${?}

checkfile "${ISOFILE}"
exitonerror ${?}

#-------------------------------------------------

if [ "${STORAGE}" = "xdisk" ]; then
  ensuredir "${XDISKDIR}"
  exitonerror ${?}
  TARGET="${XDISKDIR}/${LABEL}.usb"
  if [ "${NOFORMAT}" = "" ]; then
    # new
    if [ ! -f "${TARGET}" ]; then
      action "Creating ${TARGET}"
    elif [ "${NOFORMAT}" = "" ]; then
      action "Overwriting ${TARGET}"
      info "Press ENTER to continue, CTRL-C to abort"
      keywait
      image-detach "${TARGET}"
      exitonerror ${?}
      ensurenofile "${TARGET}"
      exitonerror ${?}
    fi
    image-create "${TARGET}" $((MIBS+10))M
    exitonerror ${?}
  fi
  checkfile "${TARGET}"
  exitonerror ${?}
  ATTACH="image"
  DISKDEV="$(image-attach "${TARGET}")"
  exitonerror ${?}
elif [ "${STORAGE}" = "vdisk" ]; then
  # TODO: new
  TARGET="$(abspath "${TARGET}")"
  len="$(stringlen "${TARGET}")"
  disk="$(endstring "${TARGET}" $((len-4)))"
  raw="$(endstring "${TARGET}" $((len-3)))"
  if [ "${disk}" = ".disk" ] || [ "${raw}" = ".raw" ] || [ "${raw}" = ".usb" ]; then
    ATTACH="image"
    DISKDEV="$(image-attach "${TARGET}" reuse)"
    exitonerror ${?}
  else
    ATTACH="vimage"
    DISKDEV="$(vimage-attach "${TARGET}" unsafe)"
    exitonerror ${?}
  fi
else
  ATTACH=""
  TARGET="$(abspath "${TARGET}")"
  DISKDEV="${TARGET}"
fi

checkdevice "${DISKDEV}"
exitonerror ${?}

#-------------------------------------------------

div

note "HOSTDIR:  ${HOSTDIR} (${WININST})"
note "TARGET:   ${TARGET} (${VARIANT})"
note "MIBS:     ${MIBS}"
note "ISOFILE:  ${ISOFILE} (source)"
if [ "${NOFORMAT}" = "" ]; then
note "LABEL:    ${LABEL}"
else
note "LABEL:    ${LABEL} [noformat]"
fi

div

info "Press ENTER or wait for 25 seconds"
keywait 25

#-------------------------------------------------
separator "ISO"

checkfile "${ISOFILE}"
exitonerror ${?}

ISODEV="$(image-attach "${ISOFILE}")"
exitonerror ${?}

note "ISO loop device: ${ISODEV}"
sleep 1

dirmount-iso "/mnt/wininst" "${ISODEV}" ro
exitonerror ${?}

if [ ! -f "/mnt/wininst/sources/install.esd" ]; then
  errorexit "Missing /mnt/wininst/sources/install.esd"
fi

note "Found sources/install.esd"

#-------------------------------------------------
separator "Format"

MAXLABEL="$(startstring "${LABEL}" 11)"

if [ "${NOFORMAT}" = "" ]; then
  # new
  disk-format "${DISKDEV}" quick mbr
  exitonerror ${?}

  PARTDEV="$(diskpart-add "${DISKDEV}" 1 4M ${MIBS}M fat32)"
  exitonerror ${?}

  format-fat32 "${PARTDEV}" "${MAXLABEL}"
  exitonerror ${?}

  diskpart-flag "${DISKDEV}" 1 boot on
  exitonerror ${?}
else
  # update
  if [ -b "${DISKDEV}p1" ]; then
    PARTDEV="${DISKDEV}p1"
  elif [ -b "${DISKDEV}1" ]; then
    PARTDEV="${DISKDEV}1"
  else
    errorexit "Missing ${DISKDEV}[p]1"
  fi
  vol-label "${PARTDEV}" "${MAXLABEL}"
  exitonerror ${?}
fi

#-------------------------------------------------
separator "Copy"

dirmount-fat "/mnt/fat32inst" "${PARTDEV}"
exitonerror ${?}

dir-copy "/mnt/wininst" "/mnt/fat32inst" plain
exitonerror ${?}

div

checkfile "/mnt/fat32inst/wininst.conf"
exitonerror ${?}

# load config again (backed by wininst.conf)

loadcommon "${STORAGE}" "/mnt/fat32inst/wininst.conf"
exitonerror ${?}

loadentity "winhost" "${HOSTDIR}"
exitonerror ${?}

note "WINHOST:   ${WINHOST}"
note "WINMIBS:   ${WINMIBS}"
note "WINUSER:   ${WINUSER}"
note "WINPASS:   ${WINPASS}"
note "WINLANG:   ${WINLANG}"
note "WINFORMAT: ${WINFORMAT}"
note "WININPUT:  ${WININPUT}"
note "WINKEY:    ${WINKEY}"

div

if [ "${WINKEY}" = "" ]; then
  warning "Blank WINKEY"
fi

info "Press ENTER or wait for 25 seconds"
keywait 25

#-------------------------------------------------

# generate autounattend.xml

UNATTENDED="/mnt/fat32inst/autounattend.xml"
TEMPLATE="${HOSTDIR}/autounattend.xml"

action "Compiling autounattend.xml"
sleep 1

cp "${TEMPLATE}" "${UNATTENDED}"
exitonerror ${?}

replaceinfile '${WINHOST}' "${WINHOST}" "${UNATTENDED}"
replaceinfile '${WINUSER}' "${WINUSER}" "${UNATTENDED}"
replaceinfile '${WINPASS}' "${WINPASS}" "${UNATTENDED}"
replaceinfile '${WINLANG}' "${WINLANG}" "${UNATTENDED}"
replaceinfile '${WINFORMAT}' "${WINFORMAT}" "${UNATTENDED}"
replaceinfile '${WININPUT}' "${WININPUT}" "${UNATTENDED}"
replaceinfile '${WINMIBS}' "${WINMIBS}" "${UNATTENDED}"
replaceinfile '${WINKEY}' "${WINKEY}" "${UNATTENDED}"

# cleanup

ensurenofile "/mnt/fat32inst/wininst.conf"
exitonerror ${?}

#-------------------------------------------------
separator "Unmount"

dirmount-remove "/mnt/wininst"
image-detach "${ISODEV}"

dirmount-remove "/mnt/fat32inst"

if [ "${ATTACH}" = "image" ]; then
  image-detach "${TARGET}"
elif [ "${ATTACH}" = "vimage" ]; then
  vimage-detach "${DISKDEV}"
fi

#-------------------------------------------------
