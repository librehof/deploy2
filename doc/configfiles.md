# Deploy 2 | Config Files

GPLv3 (C) 2023 [librehof.org](http://librehof.org) [(top)](/README.md)
<br>

### Content

- [load sequence](#load-sequence)
- [built-in](#built-in-auto-set)
- [global.conf](#globalconf-and-localconf)
- [machine.conf](#machineconf-hardware-instance)
- [host.conf](#hostconf-linux-instance)
- [winhost.conf](#winhostconf-windows-instance)  
<br>

## load sequence

Configuration is loaded in the following order...  

### common
- config/**global**.conf
- config/**local**.conf # global.conf override (not in git)
- config/global.\<variant\>
- config/local.\<variant\> # global.\<variant\> override (not in git)

### machine
- machines/\<machine\>/**machine**.conf
- machines/\<machine\>/machine.\<variant\>

### windows
- <winverarchtype\>.wininst.iso/wininst.conf (WINKEY=\<generic key\>)

### host
- host\[s|v|x|c\]/\<host\>/\[**win**\]**host**.conf
- host\[s|v|x|c\]/\<host\>/\[win\]host.\<variant\> 
<br>

## built-in (auto set)

```
STORAGE=""               # disk|vdisk|xdisk
XDISKDIR=""              # xdisk-directory = x<XVARIANT>

ENTITY=""                # machine|host|winhost
HOST=""                  # <host>
```  
<br>

## global.conf (and local.conf)

```
VARIANT="live"           # authentic variant          
XVARIANT="dev"           # x-variant => x<variant>/<host>.disk          

ARTIFACTREPO=""          # [user@host:]globalartifactdir/$(utcyear)"

VDISKDIR="/var/lib/libvirt/images"
VDISKTYPE="qcow2"

SCHEMA="ext4@efisd"      # disk schema (see README)

XDISKMIBS=<n>            # default: blank
ENDMIBS=<n>              # default: auto-sized from 100 to 16000
SWAPMIBS=<n>             # default: auto-sized from 0 to 32000
RAMTMPMIBS=<n>           # default: SWAPMIBS if >= 16000

LOGMIBS=<n>              # default: 500
TIMEZONE=""              # example: Europe/Stockholm
LOCALNTP=""              # single ntp server for stationary LAN hosts (LAN-time)

ADMIN="<user>"           # default: root
ADMINDIR="<dir>"         # default: /root or /home/<ADMIN>
ADMINLANG="en_US"
ADMINKB="us"             # see note below

WINUSER="admin"
WINPASS="123"
WINLANG="en-US"
WINFORMAT="<winlang>"    # default: same as WINLANG
WININPUT="<code>"        # see note below, default: 0409:00000409
```
For ADMINKB values try: localectl list-x11-keymap-layouts  
For WININPUT values see: [Windows Locale Table](winlocale.tab)  
<br>

## machine.conf (hardware instance)

```
HARDWARE="<model>"       # hardware model (link to hardware/<hardware>)
BIOSKEY="<key>"          # how to bring up BIOS/UEFI-menu on bootup
BOOTKEY="<key>"          # how to bring up Boot-menu on bootup

SCHEMA="<schema>"        # multifs disk schema
INSTANCES="<letters>"    # if primary/secondary: one machine per letter, like AB

DISKMIBS="<n>"           # after-the-fact documentation or size of virtual-machine disk
ENDMIBS=<n>              # free space left at the end (auto)
XDISKMIBS=<n>            # size of x-disk (see xdisk-create)
SWAPMIBS=<n>             # size of swap (auto), disable with SWAPMIBS=0
RAMTMPMIBS=<n>           # size of /tmp RAM-disk (auto), disable with RAMTMPMIBS=0

CPUTYPE="<model>"        # documentation only
CPUCORES=<n>             # documentation only
RAMMIBS=<n>              # documentation only

LANMAC=""                # example: xx:xx:xx:xx:xx:xx

TIMEZONE=""              # example: Europe/Stockholm
LOCATION="<location>"    # documentation only
INFO="<comment>"         # documentation only
```
<br>

## host.conf (linux instance)

```
MACHINE="<machine>"      # machine instance(s) (expecting machines/<machine>/machine.conf)
SCHEMA="<schema>"        # disk schema (if no MACHINE)

ROOTFS="<distroverarchtype>" # [*] artifacts/<distroverarchtype>.rootfs.sqfs
LINUXPARAMS=""           # additional kernel command line parameters

DISKMIBS=<n>             # after-the-fact documentation or size of virtual-machine disk
ENDMIBS=<n>              # free space left at the end (auto)
XDISKMIBS=<n>            # size of x-disk (see xdisk-create)
SWAPMIBS=<n>             # size of swap (auto), disable with SWAPMIBS=0
RAMTMPMIBS=<n>           # size of /tmp RAM-disk (auto), disable with RAMTMPMIBS=0

LOGMIBS=<n>              # log size (journald.conf)
TIMEZONE=""              # example: Europe/Stockholm

ADMIN="<user>"           # default: root
ADMINDIR="<dir>"         # default: /root or /home/<ADMIN>
ADMINLANG="<lang>"       # default: en_US, example: sv_SE (in addition to en_US)
ADMINKB="<code>"         # console keyboard, example: se (see /etc/default/keyboard/XKBLAYOUT)

LANDN="dn dn2 ..."       # local domain name(s) - default: "<host>"
PUBDN="dn dn2 ..."       # public domain name(s) - single entry or space-separated list
LANIP="n.n.n.n"          # if host has a known fixed local ip
PUBIP="n.n.n.n"          # if host has a known fixed public ip 
SSHIP="n.n.n.n"          # override LANIP/PUBIP for ssh
SSHPORT=<n>              # default: 22

INFO="<comment>"         # documentation only
```
[*] = mandatory  
<br>

## winhost.conf (windows instance)

```
MACHINE="<machine>"      # primary machine instance (expecting machines/<machine>/machine.conf)
WININST="<winverarchtype>" # [*] artifacts/<winverarchtype>.wininst.iso
WINKEY="a-a-a-a-a"       # will override generic license key in wininst.conf

DISKMIBS=<n>             # after-the-fact documentation or size of virtual-machine disk
XDISKMIBS=<n>            # size of x-disk (see xdisk-create)
WINMIBS=<n>              # main-partition size (to use with an appropriate autounattend.xml)

WINUSER="admin"          # default: admin
WINPASS="123"            # default: 123
WINLANG="en-US"          # default: en-US
WINFORMAT="<WINLANG>"    # default: same as WINLANG
WININPUT="<code>"        # see note below, default: 0409:00000409

LANDN="dn dn2 ..."       # local domain name(s) - default: "<host>"
PUBDN="dn dn2 ..."       # public domain name(s) - single entry or space-separated list
LANIP="n.n.n.n"          # if host has a known fixed local ip
PUBIP="n.n.n.n"          # if host has a known fixed public ip 
SSHIP="n.n.n.n"          # override LANIP/PUBIP for ssh
SSHPORT=<n>              # default: 22

INFO="<comment>"         # documentation only
```
[*] = mandatory  
<br>
