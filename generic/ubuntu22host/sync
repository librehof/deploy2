#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
cd "${SCRIPTPATH}"
. ./hostlib

#=================================================

# /deploy/sync

summary()
{
  info ""
  info "Local host sync (/deploy <=> rootfs)"
  info ""
  info "Usage:"
  info " ${SCRIPT} delta [missing|pushsame|pullsame]"
  info " ${SCRIPT} newest  # two-way"
  info " ${SCRIPT} missing # push missing"
  info " ${SCRIPT} initial # first time (no checks)"
  info " ${SCRIPT} push [<file>]"
  info " ${SCRIPT} pull [<file>]"
  info " ${SCRIPT} decrypt # /deploy => /deploy.secret"
  info " ${SCRIPT} encrypt # /deploy <= /deploy.secret"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

METHOD="${1}"
MODE="${2}"
FILE="${2}"

if [ "${METHOD}" = "initial" ]; then
  INITIAL="true"
  METHOD="push"
else
  INITIAL="false"
fi

checksuperuser
exitonerror ${?}

#------------------------------------------------

# load config

checkfile "/host.conf"
exitonerror ${?}

. "/host.conf"
exitonerror ${?}

if [ "${HOST}" = "" ]; then
  errorexit "Blank HOST in /host.conf"
fi

if [ "${VARIANT}" = "" ]; then
  errorexit "Blank VARIANT in /host.conf"
fi

#------------------------------------------------

# decrypt

decryptbase()
{
  ensuredir "${SCRIPTPATH}.secret"
  exitonerror ${?}
  chmod 700 "${SCRIPTPATH}.secret"
  ensurenodir "${SCRIPTPATH}.secret/rootfs" note
  exitonerror ${?}
  dir-restore "${SCRIPTPATH}.secret/rootfs" "rootfs.7z" password
  if [ "${?}" != 0 ]; then
    # retry
    ensurenodir "${SCRIPTPATH}.secret/rootfs"
    exitonerror ${?}
    dir-restore "${SCRIPTPATH}.secret/rootfs" "rootfs.7z" password
    exitonerror ${?}
  fi
  chmod go-rwx -R "${SCRIPTPATH}.secret/rootfs" # owner only
}

decryptvariant()
{
  ensuredir "${SCRIPTPATH}.secret"
  exitonerror ${?}
  chmod 700 "${SCRIPTPATH}.secret"
  ensurenodir "${SCRIPTPATH}.secret/rootfs.${VARIANT}" note
  exitonerror ${?}
  dir-restore "${SCRIPTPATH}.secret/rootfs.${VARIANT}" "rootfs.${VARIANT}.7z" password
  if [ "${?}" != 0 ]; then
    # retry
    ensurenodir "${SCRIPTPATH}.secret/rootfs.${VARIANT}"
    exitonerror ${?}
    dir-restore "${SCRIPTPATH}.secret/rootfs.${VARIANT}" "rootfs.${VARIANT}.7z" password
    exitonerror ${?}
  fi
  chmod go-rwx -R "${SCRIPTPATH}.secret/rootfs.${VARIANT}" # owner only
}

if [ "${METHOD}" = "decrypt" ] || [ ! -e "${SCRIPTPATH}.secret" ]; then
  if [ -f "rootfs.7z" ]; then
    decryptbase
  fi
  if [ -f "rootfs.${VARIANT}.7z" ]; then
    decryptvariant
  fi
fi

if [ "${METHOD}" = "decrypt" ]; then
  exit
fi

#------------------------------------------------

# encrypt

if [ "${METHOD}" = "encrypt" ]; then
  action "Encrypting, take your time to enter the correct password!"
  keywait 2
  if [ -d "${SCRIPTPATH}.secret/rootfs" ]; then
    ensurenofile "rootfs.7z" note
    exitonerror ${?}
    dir-save "${SCRIPTPATH}.secret/rootfs" "rootfs.7z" password
    exitonerror ${?}
  fi
  if [ -d "${SCRIPTPATH}.secret/rootfs.${VARIANT}" ]; then
    ensurenofile "rootfs.${VARIANT}.7z" note
    exitonerror ${?}
    dir-save "${SCRIPTPATH}.secret/rootfs.${VARIANT}" "rootfs.${VARIANT}.7z" password
    exitonerror ${?}
  fi
  exit
fi

#------------------------------------------------

# rootfshomedelta <parent> <secret> <ending>

rootfshomedelta()
{
  parent="${1}"
  secret="${2}"
  ending="${3}"

  # rootfs
  rootfsdir="${parent}${secret}/rootfs${ending}"
  if [ -d "${rootfsdir}" ]; then
    dir-delta "${rootfsdir}" "/" ${MODE}
  fi

  # home
  if [ "${secret}" = "" ]; then
    homedir="${parent}/home${ending}"
    if [ -d "${homedir}" ]; then
      chmod a+xr "${parent}"
      chmod a+xr "${homedir}"
      if [ "${?}" != 0 ]; then
        warning "Failed to chmod ${homedir}"
        return
      fi
      names="$(ls -1 -A "${homedir}" 2> /dev/null)"
      if [ "${?}" != 0 ]; then
        warning "Failed to list ${homedir}"
        return
      fi
      for name in $names; do
        if [ ! -d "/home/${name}" ]; then
          warning "Missing /home/${name} (create user)"
          continue
        fi
        dir-delta "${homedir}/${name}" "/home/${name}" ${MODE}
      done
    fi
  fi
}

#------------------------------------------------

# delta

if [ "${METHOD}" = "delta" ]; then
  # delta base
  rootfshomedelta "${SCRIPTPATH}" "" ""
  rootfshomedelta "${SCRIPTPATH}" ".secret" ""
  # delta variant
  rootfshomedelta "${SCRIPTPATH}" "" ".${VARIANT}"
  rootfshomedelta "${SCRIPTPATH}" ".secret" ".${VARIANT}"
  exit
fi

#------------------------------------------------

# push file

if [ "${METHOD}" = "push" ] && [ "${FILE}" != "" ]; then
  FILE="$(replaceinstring "/deploy/rootfs.${VARIANT}" "" "${FILE}")"
  FILE="$(replaceinstring "/deploy/rootfs" "" "${FILE}")"
  if [ "${FILE}" = "" ]; then
    errorexit "Path error"
  fi
  START="$(startstring "${FILE}" 1)"
  if [ "${START}" != "/" ]; then
    FILE="/${FILE}"
  fi
  STATUS=2
  if [ -f "/deploy/rootfs.${VARIANT}${FILE}" ]; then
    ensurecopy "/deploy/rootfs.${VARIANT}${FILE}" "${FILE}"
    STATUS="${?}"
  elif [ -f "/deploy/rootfs${FILE}" ]; then
    ensurecopy "/deploy/rootfs${FILE}" "${FILE}"
    STATUS="${?}"
  fi
  if [ "${STATUS}" != 0 ]; then
    DIR="$(dirname "${FILE}")"
    info "Try:"
    info " mkdir -p \"${DIR}\""
    info " cp -p \"/deploy[.secret]/rootfs[.${VARIANT}]${FILE}\" \"${FILE}\""
  fi
  exit "${STATUS}"
fi

#------------------------------------------------

# pull file

if [ "${METHOD}" = "pull" ] && [ "${FILE}" != "" ]; then
  FILE="$(replaceinstring "/deploy/rootfs.${VARIANT}" "" "${FILE}")"
  FILE="$(replaceinstring "/deploy/rootfs" "" "${FILE}")"
  if [ "${FILE}" = "" ]; then
    errorexit "Path error"
  fi
  START="$(startstring "${FILE}" 1)"
  if [ "${START}" != "/" ]; then
    FILE="/${FILE}"
  fi
  checkfile "${FILE}"
  exitonerror ${?}
  STATUS=2
  if [ -f "/deploy/rootfs.${VARIANT}${FILE}" ]; then
    ensurecopy "${FILE}" "/deploy/rootfs.${VARIANT}${FILE}"
    STATUS="${?}"
  elif [ -f "/deploy/rootfs${FILE}" ]; then
    ensurecopy "${FILE}" "/deploy/rootfs${FILE}"
    STATUS="${?}"
  fi
  if [ "${STATUS}" != 0 ]; then
    DIR="$(dirname "${FILE}")"
    info "Try:"
    info " mkdir -p \"/deploy[.secret]/rootfs[.${VARIANT}]${DIR}\""
    info " cp -p \"${FILE}\" \"/deploy[.secret]/rootfs[.${VARIANT}]${FILE}\""
  fi
  exit "${STATUS}"
fi

#------------------------------------------------

# rootfshomesync <parent> <secret> <ending>

rootfshomesync()
{
  parent="${1}"
  secret="${2}"
  ending="${3}"

  # rootfs
  rootfsdir="${parent}${secret}/rootfs${ending}"
  if [ -d "${rootfsdir}" ]; then
    dir-sync "${rootfsdir}" "/" ${METHOD}
    exitonerror ${?}
  fi

  # home
  if [ "${secret}" = "" ]; then
    homedir="${parent}/home${ending}"
    if [ -d "${homedir}" ]; then
      chmod a+xr "${parent}"
      chmod a+xr "${homedir}"
      if [ "${?}" != 0 ]; then
        warning "Failed to chmod ${homedir}"
        return
      fi
      names="$(ls -1 -A "${homedir}" 2> /dev/null)"
      if [ "${?}" != 0 ]; then
        warning "Failed to list ${homedir}"
        return
      fi
      for name in $names; do
        if [ ! -d "/home/${name}" ]; then
          warning "Missing /home/${name} (create user)"
          continue
        fi
        chown -R "${name}" "${homedir}/${name}"
        exitonerror ${?} "Failed to set owner at ${homedir}/${name}"
        runuser -u "${name}" -- dir-sync "${homedir}/${name}" "/home/${name}" ${METHOD}
        exitonerror ${?}
      done
    fi
  fi
}

#------------------------------------------------

# sync base

rootfshomesync "${SCRIPTPATH}" "" ""
rootfshomesync "${SCRIPTPATH}" ".secret" ""

# sync variant

rootfshomesync "${SCRIPTPATH}" "" ".${VARIANT}"
rootfshomesync "${SCRIPTPATH}" ".secret" ".${VARIANT}"

# additional sync

if [ "${METHOD}" != "pull" ]; then
  if [ -f "sync2" ]; then
    note "sync2"
    . "./sync2"
  fi
  if [ -f "sync3" ]; then
    note "sync3"
    . "./sync3"
  fi
fi

#------------------------------------------------

if [ "${METHOD}" = "pull" ]; then
  # encrypt after pull (note)
  if [ -d "${SCRIPTPATH}.secret" ]; then
    note "TODO?: ${SCRIPTPATH}/${SCRIPT} encrypt"
  fi
elif [ "${INITIAL}" = "false" ]; then
  # checks after sync
  checkhostname
  checklogsize
  checklocalntp
  checktimezone
fi

#------------------------------------------------
