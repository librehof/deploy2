# Deploy 2 | Deployment flows

GPLv3 (C) 2023 [librehof.org](http://librehof.org) [(top)](/README.md)
<br>

## Linux hosts - install

```mermaid
graph LR
    gitserver[(git-server)]
    operator --- gitserver
    operator --> deployer
    deployer[deployer usb]
    deployer -- initial boot --> host1
    deployer -- initial boot --> hostN
    host1[host1]
    hostN[hostN]
```

## Linux hosts - two-way update

```mermaid
graph LR
    gitserver[(git-server)]
    operator --- gitserver
    operator --- do
    do[ctl/interact]
    do --- |ssh/rsync| host1
    do --- |ssh/rsync| hostN
    host1[host1 - delta/sync]
    hostN[hostN - delta/sync]
```

## Windows hosts - install

```mermaid
graph LR
    gitserver[(git-server)]
    operator --- gitserver
    operator --> wininst1
    operator --> wininstN
    wininst1[winhost1 installer usb]
    wininstN[winhostN installer usb]
    wininst1 -- initial boot --> winhost1
    wininstN -- initial boot --> winhostN
    winhost1
    winhostN
```
