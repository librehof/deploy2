#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib

#=================================================

# host/stage

summary()
{
  info ""
  info "Custom host stage"
  info ""
  info "Usage:"
  info " ${SCRIPT} <rootdir> new|update"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

checksuperuser
exitonerror ${?}

# check base
checkdir "artifacts"
exitonerror ${?}

HOST="$(basename "${SCRIPTPATH}")"

ROOTDIR="${1}"
METHOD="${2}"

if [ "${ROOTDIR}" = "" ]; then
  inputexit
fi

checkdir "${ROOTDIR}"
exitonerror ${?}

if [ "${METHOD}" != "new" ] && [ "${METHOD}" != "update" ]; then
  inputexit
fi

#------------------------------------------------

checksecrets()
{
  if [ -d "${1}" ]; then
    cd "${1}"
    FOUND="$(ls -1 | grep ".secret")"
    cd ..
    if [ "${FOUND}" != "" ]; then
      error "Found open secret: ${1}/${FOUND}"
      sleep 1
      exit 2
    fi
  fi
}

checksecrets "hosts"
checksecrets "hostv"
checksecrets "hostx"
checksecrets "hostc"

#------------------------------------------------

# will stage <rootfs>/deployer directory

if [ "${METHOD}" = "new" ]; then
  ensurenodir "${ROOTDIR}/deployer"
  exitonerror ${?}
fi

ensuredir "${ROOTDIR}/deployer"
exitonerror ${?}

chmod 700 "${ROOTDIR}/deployer"
exitonerror ${?}

#------------------------------------------------

# selected artifacts (rootfs)

ensuredir "${ROOTDIR}/deployer/artifacts"
exitonerror ${?}

cd "artifacts"
exitonerror ${?}

note "rootfs ..."

ENTRIES="$(ls -1 *.rootfs.sqfs 2> /dev/null)"
for ENTRY in ${ENTRIES}
do
  ensurecopy "${ENTRY}" "${ROOTDIR}/deployer/artifacts/"
  exitonerror ${?}
  sync
done

note "kernel ..."

ENTRIES="$(ls -1 *-kernel-* 2> /dev/null)"
for ENTRY in ${ENTRIES}
do
  ensurecopy "${ENTRY}" "${ROOTDIR}/deployer/artifacts/"
  exitonerror ${?}
  sync
done

note "firmware ..."

ENTRIES="$(ls -1 *-firmware-* 2> /dev/null)"
for ENTRY in ${ENTRIES}
do
  ensurecopy "${ENTRY}" "${ROOTDIR}/deployer/artifacts/"
  exitonerror ${?}
  sync
done

if [ -d "firmware" ]; then
  dir-copy "firmware" "${ROOTDIR}/deployer/artifacts/firmware"
  exitonerror ${?}
  sync
fi

if [ -d "install" ]; then
  note "install ..."
  dir-copy "install" "${ROOTDIR}/deployer/artifacts/install"
  exitonerror ${?}
  sync
fi

cd ".."
exitonerror ${?}

#------------------------------------------------

# config, machines and hosts

copydir()
{
  if [ -d "${1}" ]; then
    note "${1}:"
    dir-copy "${1}" "${ROOTDIR}/deployer/${1}"
    exitonerror ${?}
  fi
}

copydir "checks"
copydir "config"
copydir "machines"
copydir "generic"
copydir "hosts"
copydir "hostv"
copydir "hostx"
copydir "hostc"

#------------------------------------------------

note "base"

if [ -f "inherit" ]; then
  ensurecopy "inherit" "${ROOTDIR}/deployer/"
fi

if [ -f "backup" ]; then
  ensurecopy "backup" "${ROOTDIR}/deployer/"
fi

if [ -f "setup" ]; then
  ensurecopy "setup" "${ROOTDIR}/deployer/"
fi

if [ -f ".gitignore" ]; then
  ensurecopy ".gitignore" "${ROOTDIR}/deployer/"
fi

if [ -d ".git" ]; then
  dir-copy ".git" "${ROOTDIR}/deployer/.git"
  exitonerror ${?}
fi

#------------------------------------------------

if [ -L "${ROOTDIR}/deployer/ctl" ]; then
  unlink "${ROOTDIR}/deployer/ctl"
  exitonerror ${?} "Failed to remove old /deployer/ctl"
fi

# deployN

ENTRIES="$(ls -1 ./ | grep "^deploy" 2> /dev/null)"
exitonerror ${?}
for ENTRY in ${ENTRIES}
do
  note "${ENTRY}"
  dir-copy "${ENTRY}" "${ROOTDIR}/deployer/${ENTRY}"
  exitonerror ${?}
  ln -s "${ENTRY}/ctl" "${ROOTDIR}/deployer/ctl"
  exitonerror ${?} "Failed to make /deployer/ctl link"
done

#------------------------------------------------

# supersysN

ENTRIES="$(ls -1 ./ | grep "^supersys" 2> /dev/null)"
exitonerror ${?}
for ENTRY in ${ENTRIES}
do
  note "${ENTRY}"
  dir-copy "${ENTRY}" "${ROOTDIR}/deployer/${ENTRY}"
  exitonerror ${?}
done

#------------------------------------------------

# remove any DHCP lease history
LEASES="$(find /var/lib -name "dhclient.leases" 2> /dev/null | tail -1)"
if [ "${LEASES}" != "" ] && [ -f "${LEASES}" ]; then
  ensurenofile "${LEASES}" note
  exitonerror ${?}
fi

chown -R root:root "${ROOTDIR}/deployer"
exitonerror ${?}

#------------------------------------------------

# add info to login message

MOTDLINES="$(cat "${ROOTDIR}/etc/motd" | wc -l)"

if [ "${MOTDLINES}" -lt 10 ]; then
  appendline "Verify current time:" "${ROOTDIR}/etc/motd"
  appendline "> timedatectl" "${ROOTDIR}/etc/motd"
  appendline "> timedatectl set-time \"YYYY-MM-DD hh:mm:ss\"" "${ROOTDIR}/etc/motd"
  appendline "Verify hw clock:" "${ROOTDIR}/etc/motd"
  appendline "> hwclock" "${ROOTDIR}/etc/motd"
  appendline "> cat /proc/driver/rtc | grep batt" "${ROOTDIR}/etc/motd"
  appendline "> hwclock --systohc # if out of sync" "${ROOTDIR}/etc/motd"
  appendline "IP from dhcp:" "${ROOTDIR}/etc/motd"
  appendline "> nic-list" "${ROOTDIR}/etc/motd"
  appendline "> nicip-obtain <nic> new [fg]" "${ROOTDIR}/etc/motd"
  appendline "List disks:" "${ROOTDIR}/etc/motd"
  appendline "> disk-list info" "${ROOTDIR}/etc/motd"
  appendline "> disk-info <device>" "${ROOTDIR}/etc/motd"
  appendline "Deploy:" "${ROOTDIR}/etc/motd"
  appendline "> cd /deployer" "${ROOTDIR}/etc/motd"
  appendline "> ctl/host-to-single <hostdir> <device> /root/admin.pub" "${ROOTDIR}/etc/motd"
  appendline "> ctl/multi-init <confdir> <device> format|add" "${ROOTDIR}/etc/motd"
  appendline "> ctl/host-to-multi <hostdir> <device> /root/admin.pub" "${ROOTDIR}/etc/motd"
  appendline "----------------------------------------------------------" "${ROOTDIR}/etc/motd"
  exitonerror ${?}
fi

#------------------------------------------------

# additional stage 2

if [ -f "stage2" ]; then
  note "stage2"
  . ./stage2
fi

# additional stage 3

if [ -f "stage3" ]; then
  note "stage3"
  . ./stage3
fi

#------------------------------------------------

success "staged"
