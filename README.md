# Deploy 2

Lightweight Host Deployment  

GPLv3 (C) 2023 [librehof.org](http://librehof.org)  
  
[Legal Notice](NOTICE)  
[Change log](doc/changelog.md) \<= see changes here  
  
## Introduction

This repo enables you to create your own multi-host deployment repo  
See: [Deployment flows](doc/depflows.md) (Linux and Windows)  

#### Linux

On linux you can capture the information needed (minus artifacts), to setup all your linux hosts from a single git repo (your single-source-of-truth deployment repo)  
  
You simply take ownership of the files you change in the host's rootfs structure, by putting them in your deployment repo structure. See: [File Structure](doc/filestructure.md) and [Config Files](doc/configfiles.md)  
  
The "lightweight" approach here means there is no "magic" translation layer in between, no plug-ins etc. 
Life-cycle handling (installation, configuration, upgrading, ...) needs to be inline with the documentation for each software package you deploy.  
  
Also the rules for reconfiguring and restarting services may be package dependent. However "systemd" is likely to be available, 
it is recommended to use it whenever possible. 

#### Windows

You may not be able to do full deployment on windows, 
but there is support to create "unattended" windows installers per host.
  
<br>

  
## Initial setup
```
# 1. Make a new deployment repo on your git server
```
```
# 2. Clone your new repo into <yournewrepo>
```
```
# 3. Clone deploy2 inside <yournewrepo>

cd <yournewrepo>
git clone --depth 2 https://gitlab.com/librehof/deploy2.git
```
```
# 4. Setup your repo (will setup default files and install supersys)

deploy2/init
```
```
# 5. Push initial files to your git server
```
```
# (6). Copy a generic host (as a beginner)

ctl/host-new deploy2/generic/<host> hosts/<yourhost>
```
```
# (7). Take a look at your personal ssh key (ed25519)

# list your keys
ls ~/.ssh

# if you have none or you are missing id_ed25519[.pub]
ssh-keygen -t id_ed25519

# or if you are paranoid (100 rounds => slow login)
ssh-keygen -a 100 -t ed25519
```

Notes:
- Use on **your own risk**
  - Test your commands thoroughly before running on any live system
  - You are fully responsible for any damage ([license](LICENSE))
- **Supersys** will be installed system-wide on your computer and all linux hosts
  - more info at [librehof.org](http://librehof.org) =\> supersys1 
  - all supersys commands will print help if `-h` is given  
<br>

  
## Environment classes

- authentic (ctl/interact)
  - STORAGE="**disk**" | "**vdisk**"  
  - default VARIANT="**live**" (final environment)
  - disk devices or virtual disk images 
- e**x**perimental (ctl/xinteract)
  - STORAGE="**xdisk**"
  - default VARIANT="**dev**" (local development environment) 
  - virtual disk images =\> x\<variant\>/\<host\>.disk  
<br>

  
## Configuration

See: [Config Files](doc/configfiles.md)  
<br>

  
## Disk Layouts

See: [Disk Layouts](doc/disklayouts.md) - partititioning/formatting schemes

SCHEMA
- single:
  - `ext4@bios` \<-- grub legacy boot
  - `ext4@efigrub` \<-- grub boot
  - `ext4@efisd` \<-- systemd-boot (default)
- multi: \<- systemd-boot
  - `btrfs@efisd`
  - `luksbtrfs@efisd` \<-- LUKS encrypted
  - `zfs@efisd`
  - `lukszfs@efisd` \<-- LUKS encrypted (requires initrd script)
  - `zfscrypt@efisd` \<-- ZFS native encryption (slow?)
<br>

  
## File Structure

See: [File Structure](doc/filestructure.md) - your deployment base directory  
<br>

  
## Networking

- general rules:
  - Ideally hosts access each other via domain name (DN), DN has priority over IP
  - The exception is when an outside operator connects into dev network via LANIP/PUBIP/SSHIP 
  - If hosts have LANDN/LANIP, it is assumed they all sit on the same LAN, including operator
  - A host can have 0..n local domain names, see [LANDN](doc/configfiles.md)
  - A host can have 0..n public domain names, see [PUBDN](doc/configfiles.md)
  - default LANDN="\<host\>", to disable LANDN =\> explicitly set LANDN="" in your host.conf 
- general accessibility (categories):
  - **lan**: A local host (blank PUBDN/PUBIP), can reach all hosts
  - **gateway**: A local+public host, can reach all hosts
  - **nolan**: A "cloud" host (blank LANDN/LANIP), can only reach other nolan and gateway hosts
    - note: with "port forwarding" on the gateway, local hosts can be selectively exposed   
<br>

  
## Inherit mechanism

- There is a common inherit script at the base `./inherit`
  - You can use it to produce common config if needed
  - At the end it will call each host's unique inherit script, see below 
- Each host has its own inherit script `host[s|v|x|c]/<host>/inherit`
  - Use it to inherit files from:
    - deployN/generic/
    - config/ 
    - host\[n\]/\<otherhost\>/ (like host 1 in a series of similar hosts 1,2,..)  
<br>

  
## Passwords

- Recommended default password: `123`, `Abc123+++` or `Abcdef123456++++++`
- The default is usually good enough for your local "dev" environment
  - if using host local NAT-network (provided by your virtual machine)
  - if having no "dev" secrets (no rootfs.7z/rootfs.dev.7z)
  - if having "dev" secrets, but your dev environment is considered safe and encrypted-at-rest  
- When setting-up "live" environment, strap script may prompt for super-secret ADMIN password
  - keep live password in an encrypted password wallet, a safe or a very secret place
- When doing host-to-\<scheme\> or deployer-to-single, use auto or give your ssh-pubkey
  - this enables you to login using ssh  
<br>

  
## Preparing artifacts

#### Linux rootfs - method I (using script)
- deployN/make/\<distroverarchtype\>/**make-rootfs**

#### Linux rootfs - method II (using iso):
- Download official distro **iso** into artifacts/
- ctl/**stage-xdisk**
  - follow instructions (install iso at xdev/stage.disk)
- ctl/**stage-to-rootfs**

#### Windows installer 
- Download official iso into artifacts/ - like Win10_21H2_English_x64.iso
- deployN/make/\<winverarchtype\>/**make-wininst**  
<br>

## New host
- `ctl/machine-new -h`
- `[ctl/machine-new ...]` # optional (multi-host)
- `ctl/host-new -h`
- `ctl/host-new ...`
- Review: `host[n]/<newhost>/host.conf`
  - Linux: 
    - `[MACHINE=...]`
    - `SCHEMA=...`
    - `ROOTFS=...`  
<br>

## Setup sequence for Linux

- Prerequisite: Empty USB-drive (32 GB+)
- Command sequence on Operator's host:
  - `[cd <repo>]`
  - `[./setup]`
  - `sudo disk-list info` # identify USB-drive
  - `ctl/deployer-to-single -h`
  - `sudo ctl/deployer-to-single ... auto`
- Move USB-drive to target host
- Optional: Boot into UEFI/BIOS and check settings
  - Check system time
  - Check UEFI/BIOS firmware version
  - You may have to disable "Secure Boot" to boot Linux
- Bootup from USB-drive (press correct F-key on boot)
- After bootup a list of useful commands will be listed
- Command sequence example for single-schema:
  ```
  cd /deployer
  nic-list # find nic
  nicip-obtain <nic> new [fg] # get ip via dhcp
  disk-list info # identify permanent host disk
  disk-info <disk>
  ctl/host-to-single -h
  ctl/host-to-single ... /root/admin.pub
  ```
- Command sequence example for multi-schema:
  ```
  cd /deployer
  nic-list # find nic
  nicip-obtain <nic> new [fg] # get ip via dhcp
  disk-list info # identify permanent host disk
  disk-info <disk>
  [ctl/multi-init ...] # first time only!
  ctl/host-to-multi -h
  ctl/host-to-multi ... /root/admin.pub
  ```  

<br>


## Setup sequence for Windows

- Prerequisite: Empty USB-drive (16 GB+)
- Prepare USB-drive (winhost-to-fat32inst)
- Bootup from prepared USB-drive (press correct F-key on boot)
- Perform the installation
- Post-installation 
  - for manual steps: create a checklist under hosts/\<winhost\>/checklist.md
  - and/or use a powershell script  
<br>

  
## Syncing after deployment (Linux)

- `ctl/interact -h`
- `ctl/interact <host> push`
- `ctl/interact <host>` # ssh to host
  - `[/deploy/setup initial]` # first time only!
  - `/deploy/sync -h`
  - `/deploy/sync delta` # check diffs
  - fix diffs (and restart services if needed)
  - `/deploy/sync delta` # check no diff
  - exit
- `ctl/interact <host> pull`
- commit changes  

<br>


## Virtual Machines on Linux (QEMU/KVM) 

### Create VM pointing to generated xdisk

TODO: describe
<br>

### Virtual networking with in-built dhcp (controlled by xml config)
```
virsh net-edit default
  <host name='<hostname>' ip='<lanip>'/>
  ...
  
virsh net-destroy default && virsh net-start default
```

### Simulate full network with own gateway (acting as dhcp server)

TODO: describe
<br>
