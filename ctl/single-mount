#!/bin/sh

IMPACT="bind"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib
. supersys
. "${SCRIPTPATH}/lib"

#=================================================

# deploy2/single-mount * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Mount single rootfs"
  info ""
  info "WARNING: Disk must NOT be in use (!)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <diskref>"
  info ""
  info " <diskref>:"
  info " - disk device (/dev)"
  info " - raw image file (.raw .disk)"
  info " - virtual image file (.qcow2 .vdi .vmdk)"
  info ""
  info "Mounts:"
  info " /mnt/rootfs"
  info ""
}

#=================================================

# input

ROOTDIR="/mnt/rootfs"

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}

TARGET="${1}"

if [ "${TARGET}" = "" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

if [ -e "${ROOTDIR}" ]; then
  errorexit "Already mounted: ${ROOTDIR}"
fi

TARGET="$(abspath "${TARGET}")"
exitonerror ${?}

if [ ! -e "${TARGET}" ]; then
  errorexit "Missing ${TARGET}"
fi

#-------------------------------------------------

len="$(stringlen "${TARGET}")"
devdir="$(startstring "${TARGET}" 5)"
xdir="$(dirname "${TARGET}")"
basedir="$(dirname "${xdir}")"

if [ "${devdir}" = "/dev/" ] || [ -b "${TARGET}" ]; then
  STORAGE="disk"
else
  if [ "${basedir}" = "$(pwd)" ]; then
    STORAGE="xdisk"
  else
    STORAGE="vdisk"
  fi
fi

loadcommon "${STORAGE}"
exitonerror ${?}

if [ "${STORAGE}" = "xdisk" ]; then
  if [ "$(abspath "${XDISKDIR}")" != "${xdir}" ]; then
    errorexit "Expected xdisk path: ${XDISKDIR}"
  fi
fi

#-------------------------------------------------

if [ "${STORAGE}" = "disk" ]; then
  DISKDEV="${TARGET}"
  ATTACH=""
else
  len="$(stringlen "${TARGET}")"
  disk="$(endstring "${TARGET}" $((len-4)))"
  raw="$(endstring "${TARGET}" $((len-3)))"
  if [ "${disk}" = ".disk" ] || [ "${raw}" = ".raw" ]; then
    note "Raw image"
    ATTACH="image"
    DISKDEV="$(image-attach "${TARGET}" reuse)"
    exitonerror ${?}
  else
    note "Virtual image"
    ATTACH="vimage"
    DISKDEV="$(vimage-attach "${TARGET}" unsafe)"
    exitonerror ${?}
  fi
fi

note "Disk: ${DISKDEV}"

checkdevice "${DISKDEV}"
exitonerror ${?}

#-------------------------------------------------

PT="$(diskpt-type "${DISKDEV}")"

if [ "${PT}" = "mbr" ]; then
  scanext4bios "${DISKDEV}"
else
  scanext4efi "${DISKDEV}"
  if [ "${ROOTDEV}" = "" ]; then
    errorexit "Missing ext4 rootfs at ${DISKDEV}"
  fi
fi

div

#-------------------------------------------------

# summary

note "TARGET:  ${TARGET} (${STORAGE})"
if [ "${EFIDEV}" = "" ]; then
  note "ROOTDEV: ${ROOTDEV}"
else
  note "EFIDEV:  ${EFIINFO}"
  note "ROOTDEV: ${ROOTINFO}"
fi

div

sleep 1

#-------------------------------------------------

# mount rootfs

if [ "${EFIDEV}" = "" ]; then
  rootfs-mount "${ROOTDIR}" "${ROOTDEV}" bind resolv
else
  rootfs-mount "${ROOTDIR}" "${ROOTDEV}" "efi=${EFIDEV}" bind resolv
fi

#-------------------------------------------------
