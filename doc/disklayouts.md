# Deploy 2 | Disk Layouts

GPLv3 (C) 2023 [librehof.org](http://librehof.org) [(top)](/README.md)
<br>

### Content

- [ext4 efi](#ext4efi)
- [ext4 bios](#ext4bios)
- [multiefi btrfs](#multiefi-btrfs)
- [multiefi luksbtrfs and lukszfs](#multiefi-luksbtrfs-and-lukszfs)
- [multiefi zfs](#multiefi-zfs)
- [multiefi zfscrypt](#multiefi-zfscrypt)
<br>

## ext4 efi

- single-boot
- partition-table = GPT

```mermaid
graph LR
  subgraph ext4host[ext4 / host]
    rootfs[rootfs with /boot and /swapfile]
  end
  subgraph fat32[fat32 / EFI]
    efi[host - EFI entry]
  end  
```
<br>

## ext4 bios

- legacy single-boot
- partition-table = MBR

```mermaid
graph LR
  subgraph ext4host[ext4 / host]
    rootfs[rootfs with /boot and /swapfile]
  end
  subgraph MBR
    table
    boot[boot code]
  end
```
<br>

## multiefi btrfs

- single/multi-boot
- partition-table = GPT
- can be added to existing Windows installation (efi)

```mermaid
graph LR
  subgraph linuxswap[linux swap]
    swap
  end
  subgraph btrfs[btrfs / multifs]
    hostfs1
    hostfsN
  end
  subgraph any
    anypartN[any fs]
  end
  subgraph fat32[fat32 / EFI]
    efi1[host1 - EFI entry]
    efiN[hostN - EFI entry]
  end  
```
<br>

## multiefi luksbtrfs and lukszfs

- single/multi-boot  
- partition-table = GPT
- password encrypted-at-rest
- can be added to existing Windows installation (efi)

```mermaid
graph LR
  subgraph luks2[luks]
    subgraph linuxswap[linux swap]
      swap
    end
  end
  subgraph luks1[luks]
    subgraph btrfs[btrfs / multifs]
      hostfs1
      hostfsN
    end
  end  
  subgraph any
    anypart[any fs]
  end
  subgraph fat32[fat32 / EFI]
    efi1[host1 - EFI entry]
    efiN[hostN - EFI entry]
  end  
```
<br>

## multiefi zfs

- single/multi-boot
- partition-table = GPT
- can be added to existing Windows installation (efi)

```mermaid
graph LR
  subgraph linuxswap[linux swap]
    swap
  end
  subgraph zfs[zfs / multifs]
    hostfs1
    hostfsN
  end
  subgraph any
    anypartN[any fs]
  end
  subgraph fat32[fat32 / EFI]
    efi1[host1 - EFI entry]
    efiN[hostN - EFI entry]
  end  
```
<br>

## multiefi zfscrypt

- single/multi-boot  
- partition-table = GPT
- password encrypted-at-rest
- can be added to existing Windows installation (efi)

```mermaid
graph LR
  subgraph luks2[luks]
    subgraph linuxswap[linux swap]
      swap
    end
  end
  subgraph zfs[zfscrypt / multifs]
    hostfs1
    hostfsN
  end
  subgraph any
    anypart[any fs]
  end
  subgraph fat32[fat32 / EFI]
    efi1[host1 - EFI entry]
    efiN[hostN - EFI entry]
  end  
```
<br>
