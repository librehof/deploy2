#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib
. "${SCRIPTPATH}/lib"

#=================================================

# deploy2/rootfs-strap * GPLv3 (C) 2023 librehof.org

summary()
{
  info ""
  info "Strap mounted rootfs: chroot host/strap + pubkey"
  info ""
  info "Usage:"
  info " ${SCRIPT} <hostdir> <rootdir> <storage> <schema> [<pubfile>]"
  info ""
  info "Mount: /mnt/squashfs"
  info ""
  info "Run after rootfs-init and rootfs-bootmounts"
  info "Do not bind rootfs"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

checksuperuser
exitonerror ${?}

HOSTDIR="${1}"
ROOTDIR="${2}"
STORAGE="${3}"
SCHEMA="${4}"
PUBFILE="${5}"

if [ "${HOSTDIR}" = "" ] || [ "${ROOTDIR}" = "" ] || [ "${STORAGE}" = "" ] || [ "${SCHEMA}" = "" ]; then
  inputexit
fi

if [ "${6}" != "" ]; then
  inputexit
fi

HOSTDIR="$(abspath "${HOSTDIR}")"
exitonerror ${?}

checkdir "${HOSTDIR}"
exitonerror ${?}

ROOTDIR="$(abspath "${ROOTDIR}")"
exitonerror ${?}

checkdir "${ROOTDIR}"
exitonerror ${?}

rootfs-release "${ROOTDIR}"

dirmount-remove "/mnt/squashfs"

if [ "${PUBFILE}" != "" ]; then
  PUBFILE="$(abspath "${PUBFILE}")"
  exitonerror ${?}
  PUBKEY="$(loadline "${PUBFILE}")"
  exitonerror ${?}
  if [ "${PUBKEY}" = "" ]; then
    errorexit "Blank pubfile: ${PUBFILE}"
  fi
fi

#-------------------------------------------------

loadcommon "${STORAGE}"
exitonerror ${?}

loadentity "host" "${HOSTDIR}"
exitonerror ${?}

#-------------------------------------------------

# admin key

if [ "${PUBFILE}" != "" ]; then
  if [ -e "${HOSTDIR}/rootfs${ADMINDIR}/.ssh/authorized_keys" ]; then
    note "Found admin auth file: rootfs${ADMINDIR}/.ssh/authorized_keys"
    sleep 1
  fi
  if [ -e "${HOSTDIR}/rootfs.${VARIANT}${ADMINDIR}/.ssh/authorized_keys" ]; then
    note "Found admin auth file: rootfs.${VARIANT}${ADMINDIR}/.ssh/authorized_keys"
    sleep 1
  fi
fi

if [ "${PUBKEY}" != "" ]; then
  note "Copying admin key"
  ensurecopy "${PUBFILE}" "${ROOTDIR}/root/admin.pub"
  exitonerror ${?}
fi

#-------------------------------------------------

if [ -f "${HOSTDIR}/strap" ]; then

  note "[inner strap]"

  GROUP="$(dirname "${HOSTDIR}")"
  GROUP="$(basename "${GROUP}")"

  note "/deploy/strap ${STORAGE} ${SCHEMA} ${GROUP}"

  sleep 2

  rootfs-bind "${ROOTDIR}" resolv
  exitonerror ${?}

  rootfs-run "${ROOTDIR}" "/deploy/strap ${STORAGE} ${SCHEMA} ${GROUP}"
  exitonerror ${?}

  rootfs-release "${ROOTDIR}"
  exitonerror ${?}

fi

#-------------------------------------------------

if [ -f "${HOSTDIR}/sync" ]; then

  note "[sync]"
  sleep 1

  rootfs-run "${ROOTDIR}" "/deploy/sync initial"
  if [ "${?}" != 0 ]; then
    note "Failed, trying again ..."
    sleep 1
    rootfs-run "${ROOTDIR}" "/deploy/sync initial"
    if [ "${?}" != 0 ]; then
      note "Failed to complete /deploy/sync"
      info "Press ENTER to continue, CTRL-C to abort"
      keywait
    fi
  fi

fi

#-------------------------------------------------

# admin home

if [ ! -d "${ROOTDIR}${ADMINDIR}" ]; then
  errorexit "Missing ${ROOTDIR}${ADMINDIR}"
fi

if [ "${PUBKEY}" != "" ]; then
  ensuredir "${ROOTDIR}${ADMINDIR}/.ssh"
  exitonerror ${?}
  ensurefile "${ROOTDIR}${ADMINDIR}/.ssh/authorized_keys"
  exitonerror ${?}
  FOUND="$(grep "${PUBKEY}" "${ROOTDIR}${ADMINDIR}/.ssh/authorized_keys")"
  if [ "${FOUND}" = "" ]; then
    action "Adding ssh key to ${ADMINDIR}/.ssh/authorized_keys"
    appendline "${PUBKEY}" "${ROOTDIR}${ADMINDIR}/.ssh/authorized_keys"
    exitonerror ${?}
  fi
  chmod 700 "${ROOTDIR}${ADMINDIR}/.ssh"
  chmod 600 "${ROOTDIR}${ADMINDIR}/.ssh/authorized_keys"
fi

#-------------------------------------------------
